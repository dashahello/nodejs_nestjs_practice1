import { IsEnum, IsNotEmpty, IsString, Length } from 'class-validator';

import { UserRoleEnum } from '@enums/users/roles';

export class UsersRequestDto {
  @IsString()
  @IsNotEmpty()
  @IsEnum(UserRoleEnum)
  readonly role: UserRoleEnum;

  @IsString()
  @IsNotEmpty()
  @Length(1, 100, { message: 'name must be between 1 and 100 characters' })
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  @Length(1, 100, { message: 'email must be between 1 and 100 characters' })
  readonly email: string;
}
