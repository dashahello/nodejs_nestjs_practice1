import { UserRoleEnum } from '@enums/users/roles';

import { UserInterface } from '@entities/user/interfaces/user.interface';

export class UsersResponseDto {
  readonly id?: number;

  readonly role?: UserRoleEnum;

  readonly name?: string;

  readonly email?: string;

  constructor(data?: Partial<UserInterface>) {
    if (data) {
      this.role = data.role;
      this.name = data.name;
      this.email = data.email;
    }
  }
}
