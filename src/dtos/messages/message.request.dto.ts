import { IsNotEmpty, IsString, Length } from 'class-validator';

export class MessageRequestDto {
  @IsString()
  @IsNotEmpty()
  @Length(1, 250, { message: 'something wrong with message text' })
  readonly text: string;
}
