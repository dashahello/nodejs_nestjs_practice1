import { MessageInterface } from '@entities/message/interfaces/message.interface';

export class MessageResponseDto {
  readonly id?: number;

  readonly userId?: string;

  readonly text?: string;

  constructor(data?: Partial<MessageInterface>) {
    if (data) {
      this.text = data.text;
    }
  }
}
