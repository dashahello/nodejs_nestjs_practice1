import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Unique,
  PrimaryGeneratedColumn
} from 'typeorm';

import { MessageInterface } from './interfaces/message.interface';

@Entity()
export class Message implements MessageInterface {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 100, nullable: false, unique: false })
  userId: number;

  @Column({ type: 'varchar', length: 200, nullable: false })
  text: string;

  @CreateDateColumn({ update: false })
  createdAt: Date;

  @UpdateDateColumn({ update: true })
  updatedAt: Date;
}
