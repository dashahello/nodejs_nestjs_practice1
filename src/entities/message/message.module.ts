import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { MessagesService } from './messages.service';
import { MessageRepository } from './message.repository';

@Module({
  imports: [TypeOrmModule.forFeature([MessageRepository])],
  providers: [MessagesService],
  exports: [MessagesService]
})
export class MessageEntityModule {}
