import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { MessageInterface } from './interfaces/message.interface';

import { MessageRepository } from './message.repository';

@Injectable()
export class MessagesService {
  constructor(
    @InjectRepository(MessageRepository)
    private messageRepository: MessageRepository
  ) {}

  async getMessages(userId: string): Promise<MessageInterface[]> {
    return await this.messageRepository.find({
      where: { userId }
    });
  }

  async getMessageById(id: number): Promise<MessageInterface> {
    return await this.messageRepository.findOne({
      where: { id }
    });
  }

  async createMessage(
    message: Partial<MessageInterface>,
    userId: number
  ): Promise<MessageInterface> {
    return await this.messageRepository.save({
      userId: userId,
      text: message.text
    });
  }

  async deleteMessage(id: number): Promise<void> {
    await this.messageRepository.delete(id);
  }
}
