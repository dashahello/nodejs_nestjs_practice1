export interface MessageInterface {
  id?: number;
  userId: number;
  text: string;
  createdAt: Date;
  updatedAt: Date;
}
