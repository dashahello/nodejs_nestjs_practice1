import { UserRoleEnum } from '@enums/users/roles';

export interface UserInterface {
  id?: number;
  name: string;
  email?: string;
  role: UserRoleEnum;
  createdAt: Date;
  updatedAt: Date;
}
