import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';

import { UserRoleEnum } from '@enums/users/roles';

import { UserInterface } from './interfaces/user.interface';

@Entity()
@Unique('email', ['email'])
export class User implements UserInterface {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 100, nullable: false, unique: false })
  name: string;

  @Column({ type: 'varchar', length: 100, nullable: false })
  email: string;

  @Column({ type: 'enum', enum: UserRoleEnum, default: UserRoleEnum.USER })
  role: UserRoleEnum;

  @CreateDateColumn({ update: false })
  createdAt: Date;

  @UpdateDateColumn({ update: true })
  updatedAt: Date;
}
