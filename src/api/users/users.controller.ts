import {
  Get,
  Post,
  Body,
  Param,
  Query,
  Delete,
  HttpCode,
  Controller,
  ConflictException,
  BadRequestException,
  NotFoundException
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { UsersService } from '@entities/user/users.service';

import { UsersRequestDto, UsersResponseDto } from '@dtos/users';

@Controller('users')
export class UsersController {
  constructor(
    private userService: UsersService,
    private configService: ConfigService
  ) {}

  @Get()
  async findAll(
    @Query('page') page: number,
    @Query('limit') limit: number
  ): Promise<UsersResponseDto[]> {
    const users = await this.userService.getUsers(page, limit);
    return users.map((user) => new UsersResponseDto(user));
  }

  @Get(':userId')
  async findOne(@Param('userId') id: number): Promise<UsersResponseDto> {
    const user = await this.userService.getUserById(id);

    if (user) {
      return new UsersResponseDto(user);
    }

    throw new NotFoundException();
  }

  @Post()
  @HttpCode(201)
  async create(@Body() data: UsersRequestDto): Promise<UsersResponseDto> {
    try {
      const user = await this.userService.createUser(data);
      return new UsersResponseDto(user);
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('User with this email already exists');
      } else {
        throw new BadRequestException();
      }
    }
  }

  @Delete(':userId')
  @HttpCode(204)
  async delete(@Param('userId') id: number): Promise<void> {
    if (this.configService.get('APP_PRODUCTION')) {
      await this.userService.deleteUser(id);
    }
  }
}
