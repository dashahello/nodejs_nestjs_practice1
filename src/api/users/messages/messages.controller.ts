import { MessageRequestDto, MessageResponseDto } from '@dtos/messages';
import { MessagesService } from '@entities/message/messages.service';
import {
  Get,
  Post,
  Body,
  Param,
  Delete,
  HttpCode,
  Controller,
  ConflictException,
  BadRequestException,
  NotFoundException
} from '@nestjs/common';

@Controller('users/:userId/messages')
export class MessageController {
  constructor(private messageService: MessagesService) {}

  @Get()
  async getAll(@Param('userId') userId: string): Promise<MessageResponseDto[]> {
    const messages = await this.messageService.getMessages(userId);
    return messages.map((message) => new MessageResponseDto(message));
  }

  @Get(':messageId')
  async findOne(
    @Param('messageId') messageId: number
  ): Promise<MessageResponseDto> {
    const message = await this.messageService.getMessageById(messageId);

    if (message) {
      return new MessageResponseDto(message);
    }
    throw new NotFoundException();
  }

  @Post()
  @HttpCode(201)
  async create(
    @Body() data: MessageRequestDto,
    @Param('userId') userId: number
  ): Promise<MessageResponseDto> {
    try {
      const message = await this.messageService.createMessage(data, userId);
      return new MessageResponseDto(message);
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('User with this email already exists');
      } else {
        throw new BadRequestException();
      }
    }
  }

  @Delete(':messageId')
  @HttpCode(204)
  async delete(@Param('messageId') id: number): Promise<void> {
    await this.messageService.deleteMessage(id);
  }
}
