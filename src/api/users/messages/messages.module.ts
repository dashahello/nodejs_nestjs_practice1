import { Module } from '@nestjs/common';

import { MessageEntityModule } from '@entities/message/message.module';

import { MessageController } from './messages.controller';


@Module({
  imports: [MessageEntityModule],
  controllers: [MessageController]
})
export class MessageApiModule {}
