import { Module } from '@nestjs/common';

import { MessageApiModule } from './users/messages/messages.module';

import { UsersApiModule } from './users/users.module';

@Module({
  imports: [UsersApiModule, MessageApiModule]
})
export class ApiModule {}
