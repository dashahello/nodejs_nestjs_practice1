import { getConnectionOptions } from 'typeorm';

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UsersEntityModule } from '@entities/user/user.module';
import { MessageEntityModule } from '@entities/message/message.module';

@Module({
  imports: [
    UsersEntityModule,
    MessageEntityModule,
    TypeOrmModule.forRootAsync({
      useFactory: async () =>
        Object.assign(await getConnectionOptions(), {
          autoLoadEntities: true
        })
    })
  ]
})
export class DatabaseModule {}
